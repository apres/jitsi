# Jitsi
**Jitsi** est un logiciel de visoconférence, comme **Zoom**, mais contrairement
à ce dernier il est libre et tout le monde peut l'utiliser sans frais. C'est par 
exemple ce qu'a fait le Parti Pirate en l'installant sur des ordinateurs 
constamment connectés à internet, on dit qu'il a créé une *instance* de Jitsi. 

## D'abord s'installer confortablement

Pour se mettre à l'aise on va se créer un salon en se rendant sur une 
[instance](https://jitsi.hadoly.fr/). Si cette instance ne marche plus,
un [site]( https://framatalk.org/accueil/fr/info/) recense les instances 
françaises. Il suffit alors de cliquer sur 
le bouton *Go* et vous voila dans votre salon perso. Si on veut
communiquer avec les autres il faudra autoriser votre navigateur à accéder au 
micro et à la caméra de votre ordinateur. Un simple clic sur un bouton *allow* 
suffira dans la plupart des cas. Pour inviter des gens  dans votre salon
il suffit de partager l'URL. 

On vous demandera dans la plupart des cas, le pseudo que vous voulez et si 
vous voulez arriver micro/caméra actifs ou non. 
Dans d'autres cas, un simple clic sur votre écran vous permettra de changer de
pseudo et vous ne serez peut-être pas seul(e) dans le salon, 
et si votre micro ou votre caméra sont actifs d'office
pas de panique on vous apprend à les désactiver juste après. 
Mais merci de **lire ce guide en entier** pour garantir une expérience agréable 
à toutes et tous. 

## Les boutons importants

Il se situent en bas de l'écran. Les trois boutons du centre
qui sont les plus importants. Les boutons **Micro** et **Vidéo**
désactives/actives ceux-ci enfin le bouton central permet de se **Déconnecter**.

Sur la gauche il y a trois autres boutons très utiles:

- **Partager son écran**: montre son écran aux autres participants
de la discussion
- **Bulle de discussion**:  ouvre un chat pour discuter entre participants
- **Lever la main**: demande la parole

Enfin il y a trois boutons sur la droite:

- **Mosaique**: voir tous les participants
- **Bouclier**: permet de définir un mot de passe au salon.
- **Plus d'options**: intéressant pour les utilisateurs avancés.

## Pour éviter la cacophonie
Toutes les règles de politesse s'appliquent. La plus importante et celle aussi 
le plus souvent transgressée: **on ne coupe pas la parole**. Sinon plus 
spécifiquement:

- **Couper votre micro un maximum**. Une discussion avec un grand nombre de personnes
devient vite une très bruyante rapidement. Il suffit de parler en maintenant
**la barre espace***, cela active puis coupe le micro.
- **Lever votre main pour demander la parole**. Ainsi on ne coupe pas la parole. 
Il suffit d'appuyer sur la touche **R** pour la demander. Une icône **bleue en 
forme de main** apparaît alors dans le coin en haut à gauche de votre écran.

Un indiividu sera chargé d'animer la discussion et peut utiliser ce symbole pour vous donnez la parole. Il faut en effet désigner une ou des personnes qui vont s'occuper de l'animation consistant à donner la parole et à passer d'un sujet à l'autre. Pour la désignation plusieurs possibilitées:
* C'est une ou un volontaire qui va animer la discussion.
* On élit une personne pour le faire.
* On anime à tour de rôle.

## Les possibilités offertes
Le partage d'écran (raccourci **d**) permet par exemple de désigner une personne qui anime en 
utilisant la chance ou le vote. Pour ce faire on ouvrira une nouvelle fenêtre dee navigateur pour
aller ensuite sur congressus qui permettra le vote ou sur 
[RANDOM.org](https://www.random.org/integers/) qui permettra de tirer un nombre 
entier entre 1 et le nombre de personnes présentes. 
Le partage d'écran garantira la transparence des procédures.

Il y a un timer en haut de l'écran qui peut permettre de faire tourner
régulièrement le rôle d'animation (toutes les 20 min ou moins si la personne
démissione avant). Pour faire tourner le rôle d'animation on peut utiliser 
l'ordre alphabetique des pseudos.

Enfin l'usage de la caméra permettra d'utiliser la
[gestuelle de réunion](https://fr.wikipedia.org/wiki/Gestuelle_de_r%C3%A9union) 
qui permet de faciliter les échanges dans un groupe.

Bonne discussion!
